module gitlab.com/pointerofnull/hydrusapi-go

go 1.14

require (
	github.com/dghubble/sling v1.3.0
	gopkg.in/go-playground/validator.v10 v10.3.0
)
