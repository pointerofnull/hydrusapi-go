// Package hydrusapi implements the v11 hydrus api(https://hydrusnetwork.github.io/hydrus/help/client) in golang
package hydrusapi

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gopkg.in/go-playground/validator.v10"

	"github.com/dghubble/sling"
)

const baseURL = "http://127.0.0.1:45869/"

const apiVersionCompat = 13

var validate *validator.Validate

//PageType is a page type
type PageType int

//PermissionType is a permission type
type PermissionType int

const (
	//Page types
	pageTypeUndefined         PageType = 0
	pageTypeGalleryDownloader PageType = 1
	pageTypeSimpleDownloader  PageType = 2
	pageTypeHarddriveImport   PageType = 3
	pageTypePetitions         PageType = 5
	pageTypeFileSearch        PageType = 6
	pageTypeURLDownloader     PageType = 7
	pageTypeDuplicates        PageType = 8
	pageTypeThreadWatcher     PageType = 9
	pageTypePageOfPages       PageType = 10
	// Basic permission types
	permissionTypeImportURLS     PermissionType = 0
	permissionTypeImportFiles    PermissionType = 1
	permissionTypeAddTags        PermissionType = 2
	permissionTypeSearchForFiles PermissionType = 3
	permissionTypeManagePages    PermissionType = 4
	permissionTypeManageCookies  PermissionType = 5
	//Tag actions
	actionTypeAddToTagService           = 0
	actionTypeDeleteFromTagService      = 1
	actionTypePendToTagRepo             = 2
	actionTypeRecindPendFromTagRepo     = 3
	actionTypePetitionFromTagRepo       = 4 // special
	actionTypeRecindPetitionFromTagRepo = 5
	//URL Types
	urlTypePostURL      = 0
	urlTypeFileURL      = 1
	urlTypeGalleryURL   = 3
	urlTypeWatchableURL = 4
	urlTypeUnknownURL   = 5
	// File Status
	fileStatusReadyToImport     = 0
	fileStatusSuccessfulImport  = 1
	fileStatusAlreadyInDB       = 2
	fileStatusPreviouslyDeleted = 3
	fileStatusFailedImport      = 4
	fileStatusVetoed            = 7
)

// APIVersion number of hydrus client api
type APIVersion struct {
	Version int `json:"version"`
}

// RequestNewPermissions <<
type RequestNewPermissions struct {
	AccessKey string `json:"access_key"`
}

//RequestSessionKey >>
type RequestSessionKey struct {
	SessionKey string `json:"session_key"`
}

// VerifyAccessKey verifies access key
type VerifyAccessKey struct {
	BasicPermissions []PermissionType `json:"basic_permissions"`
	HumanDescription string           `json:"human_description"`
}

// TagServices is a list of tag services on client
type TagServices struct {
	LocalTags       []string `json:"local_tags"`
	TagRepositories []string `json:"tag_repositories"`
}

// URLInfo info about how url will be parsed by hydrus
type URLInfo struct {
	NormalizedURL string `json:"normalized_url"`
	URLType       int    `json:"url_type"`
	URLTypeString string `json:"url_type_string"`
	MatchName     string `json:"match_name"`
	CanParse      bool   `json:"can_parse"`
}

// URLFiles info about files asscoiated to url
type URLFiles struct {
	NormalizedURL   string `json:"normalized_url"`
	URLFileStatuses []struct {
		Status int    `json:"status"`
		Hash   string `json:"hash"`
		Note   string `json:"note"`
	} `json:"url_file_statuses"`
}

//AddFile info about added file
type AddFile struct {
	Status int    `json:"status"`
	Hash   string `json:"hash"`
	Note   string `json:"note"`
}

// AddURL is a struct
type AddURL struct {
	HumanResultText string `json:"human_result_text"`
	NormalizedURL   string `json:"normalized_url"`
}

//FileMetaData contains file metadata
type FileMetaData struct {
	Metadata []struct {
		FileID            int         `json:"file_id"`
		Hash              string      `json:"hash"`
		Size              int         `json:"size"`
		Mime              string      `json:"mime"`
		Ext               string      `json:"ext"`
		Width             int         `json:"width"`
		Height            int         `json:"height"`
		Duration          *int        `json:"duration"`
		HasAudio          bool        `json:"has_audio"`
		NumFrames         *int        `json:"num_frames"`
		NumWords          *int        `json:"num_words"`
		KnownUrls         interface{} `json:"known_urls"`
		DetailedKnownUrls []struct {
			NormalisedURL string `json:"normalised_url"`
			URLType       int    `json:"url_type"`
			URLTypeString string `json:"url_type_string"`
			MatchName     string `json:"match_name"`
			CanParse      bool   `json:"can_parse"`
		} `json:"detailed_known_urls,omitempty"`
		ServiceNamesToStatusesToTags interface{} `json:"service_names_to_statuses_to_tags,omitempty"`
	} `json:"metadata"`
}

// SearchFiles contains results of file search
type SearchFiles struct {
	FileIds []int `json:"file_ids"`
}

// CleanedTags contains cleaned tags
type CleanedTags struct {
	Tags []string `json:"tags"`
}

//AssociateURL is return status of
type AssociateURL interface{}

//AddTags is return status
type AddTags interface{}

//RequestNewPermissionsParams is a struct containing params for api call
type RequestNewPermissionsParams struct {
	Name             string   `json:"name"`              // Name associated with new access key
	BasicPermissions []string `json:"basic_permissions"` // requested permissions
	// 		[]string{"0", "1", "2", "3"}
	//    0 - Import URLs
	//    1 - Import Files
	//    2 - Add Tags
	//    3 - Search for files

}

//FileMetaDataParams is a struct containing parameters for api call
type FileMetaDataParams struct {
	FileIds                 []string `json:"file_ids"`
	Hashes                  []string `json:"hashes"`
	OnlyReturnIdentifiers   bool     `json:"only_return_identifiers,omitempty"`  // defaults false
	DetailedFURLInformation bool     `json:"detailed_url_information,omitempty"` //defaults false
}

//FileSearchParams is a struct for parameters for file search
type FileSearchParams struct {
	Tags          []string `json:"tags"`
	SystemInbox   bool     `json:"system_inbox"`
	SystemArchive bool     `json:"system_archive"`
}

//CleanedTagsParams yeah
type CleanedTagsParams struct {
	Tags []string `json:"tags"`
}

/* AddURLParams is a struct for params for adding a url

{
	"url" : "https://8ch.net/tv/res/1846574.html",
	"destination_page_name" : "kino zone",
	"service_names_to_tags" : {
		"local tags" : [ "as seen on /tv/" ]
	}
}*/
type AddURLParams struct {
	URL                 string      `json:"url"`
	DestinationPageName string      `json:"destination_page_name"`
	ShowDestinationPage bool        `json:"show_destination_page"`
	ServiceNamesToTags  interface{} `json:"service_names_to_tags"`
}

//AssociateURLParams params needed to associate URL to hash
type AssociateURLParams struct {
	URLToAdd  string   `json:"url_to_add,omitempty" validate:"url"`
	URLsToAdd []string `json:"urls_to_add,omitempty" validate:"url"`
	URLToDel  string   `json:"url_to_del,omitempty" validate:"url"`
	URLsToDel []string `json:"urls_to_del,omitempty" validate:"url"`
	Hash      string   `json:"hash,omitempty" validate:"hexidecimal"`
	Hashes    []string `json:"hashes,omitempty" validate:"hexidecimal"`
}

//AddTagsParams params needed to add tags to files
//0 - Add to a local tag service.
//1 - Delete from a local tag service.
//2 - Pend to a tag repository.
//3 - Rescind a pend from a tag repository.
//4 - Petition from a tag repository. (This is special)
//5 - Rescind a petition from a tag repository.
/*
{
	"hash" : "df2a7b286d21329fc496e3aa8b8a08b67bb1747ca32749acb3f5d544cbfc0f56",
	"service_names_to_actions_to_tags" : {
		"my tags" : {
			"0" : [ "character:supergirl", "rating:safe" ],
			"1" : [ "character:superman" ]
		},
		"public tag repository" : {
			"2" : [ "character:supergirl", "rating:safe" ],
			"3" : [ "filename:image.jpg" ],
			"4" : [ [ "creator:danban faga", "typo" ], [ "character:super_girl", "underscore" ] ]
			"5" : [ "skirt" ]
		}
	}
}

{
	"hashes" : [ "df2a7b286d21329fc496e3aa8b8a08b67bb1747ca32749acb3f5d544cbfc0f56", "f2b022214e711e9a11e2fcec71bfd524f10f0be40c250737a7861a5ddd3faebf" ],
	"service_names_to_tags" : {
		"my tags" : [ "process this" ],
		"public tag repository" : [ "creator:dandon fuga" ]
	}
}
*/
type AddTagsParams struct {
	Hash                        []string    `json:"hash"`                                       //sha256 hash
	ServiceNamesToActionsToTags interface{} `json:"service_names_to_actions_to_tags,omitempty"` //map[string][]string
	ServiceNamesToTags          interface{} `json:"service_names_to_tags,omitempty"`            //map[string][]string
}

// AddFileParams is a struct for params for adding a file
type AddFileParams struct {
	Path string `json:"path"`
}

//CurrentPages is a struct containing all current pages on client.
type CurrentPages struct {
	Pages struct {
		Name     string `json:"name"`
		PageType int    `json:"page_type"`
		Selected bool   `json:"selected"`
		Pages    []struct {
			Name     string `json:"name"`
			PageType int    `json:"page_type"`
			Selected bool   `json:"selected"`
			Pages    []struct {
				Name     string `json:"name"`
				PageKey  string `json:"page_key"`
				PageType int    `json:"page_type"`
				Selected bool   `json:"selected"`
			} `json:"pages,omitempty"`
		} `json:"pages,omitempty"`
	} `json:"pages,omitempty"`
}

//PageFocusParam is a struct
type PageFocusParam struct {
	PageKey string `json:"page_key"`
}

// DomainCookies is a struct containing cookies of domain
type DomainCookies struct {
	Cookies [][]interface{} `json:"cookies"`
}

//URLService is a method to get URL related API calls
type URLService struct {
	sling *sling.Sling
}

//FileService is a method to get file related API calls
type FileService struct {
	sling *sling.Sling
}

//TagService is a method to get tag related API calls
type TagService struct {
	sling *sling.Sling
}

//UtilsService is a method to get various utility API Calls
type UtilsService struct {
	sling *sling.Sling
}

//ManageCookiesService is a method to get cookie related API calls
type ManageCookiesService struct {
	sling *sling.Sling
}

//ManagePagesService is a method to get pages related API calls
type ManagePagesService struct {
	sling *sling.Sling
}

//HydrusError represents a hydrus API error response
type HydrusError struct {
	Message string
	Errors  struct {
		Function string
		Field    string
		Code     int
		State    bool
		APIURI   string
	}
}

func (e HydrusError) Error() string {
	return fmt.Sprintf("Hydrus API Error: %v %+v", e.Message, e.Errors)
}

// NewURLService returns a new URLService.
func NewURLService(httpClient *http.Client) *URLService {
	return &URLService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewFileService returns a new FileService.
func NewFileService(httpClient *http.Client) *FileService {
	return &FileService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewTagService returns a new TagService.
func NewTagService(httpClient *http.Client) *TagService {
	return &TagService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewUtilsService returns a new UtilsService.
func NewUtilsService(httpClient *http.Client) *UtilsService {
	return &UtilsService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewManagePagesService returns a new ManagedPagesService
func NewManagePagesService(httpClient *http.Client) *ManagePagesService {
	return &ManagePagesService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewManageCookiesService returns a new ManageCookiesService
func NewManageCookiesService(httpClient *http.Client) *ManageCookiesService {
	return &ManageCookiesService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// GetAPIVersion returns API version number
func (s *UtilsService) GetAPIVersion() (APIVersion, *http.Response, HydrusError) {
	versionNum := new(APIVersion)
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	resp, err := s.sling.New().Path("api_version").Receive(versionNum, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			hydrusError.Errors.Function = "GetAPIVersion()"
			hydrusError.Message = fmt.Sprintf("Failed ioutil.ReadAll(): %v", err)
			hydrusError.Errors.State = false
		}
		//fixme
		fmt.Println("APIVersion: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = "GetAPIVersion()"
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Message = fmt.Sprintf("Query failed with %d: %v\n", hydrusError.Errors.Code, resp.Body)
		hydrusError.Errors.State = false
	}
	return *versionNum, resp, *hydrusError
}

// RequestPermissions requests and returns new accesskey
// accesskey key is hash from hydrus client
//TODO: There be dragons
func (s *UtilsService) RequestPermissions(params RequestNewPermissionsParams) (RequestNewPermissions, *http.Response, error) {
	accessKey := new(RequestNewPermissions)
	hydrusError := new(HydrusError)
	var urlParams string
	if len(params.BasicPermissions) != 0 {
		s := strings.Join(params.BasicPermissions, `","`)
		urlParams = "[" + fmt.Sprintf(`"%s"`, s) + "]"
		urlParams = url.QueryEscape(urlParams)
		urlParams = "&basic_permissions=" + urlParams
	}
	finalURL := fmt.Sprintf("%srequest_new_permissions?name=%s%s", baseURL, url.QueryEscape(params.Name), urlParams)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).Receive(accessKey, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		} else {

			fmt.Printf("RequestPermissions: %s\n", string(responseData))
		}
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Permissions Request failed with Status Code: %v\n", resp.StatusCode)
	}
	return *accessKey, resp, hydrusError
}

// GetSessionKey returns a session key
// accesskey key is hash from hydrus client
func (s *UtilsService) GetSessionKey(accessKey string) (RequestSessionKey, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	sessionKey := new(RequestSessionKey)
	hydrusError := new(HydrusError)
	resp, err := s.sling.New().Path(fmt.Sprintf("session_key?Hydrus-Client-API-Access-Key=%s", accessKey)).Receive(sessionKey, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("AccessKey: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *sessionKey, resp, hydrusError
}

// GetAccessKeyStatus returns the status of provided access key
// accessKey is hash from hydrus client
func (s *UtilsService) GetAccessKeyStatus(accessKey string) (VerifyAccessKey, *http.Response, HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function GetAccessKeyStatus(%s)", accessKey)
		hydrusError.Errors.State = false
	}
	accessKeyStatus := new(VerifyAccessKey)
	resp, err := s.sling.New().Path(fmt.Sprintf("verify_access_key?Hydrus-Client-API-Access-Key=%s", accessKey)).Receive(accessKeyStatus, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			hydrusError.Errors.Function = "GetAccessKeyStatus()"
			hydrusError.Message = fmt.Sprintf("Failed ioutil.ReadAll(): %v", err)
			hydrusError.Errors.State = false
		}

		fmt.Println("AccessKey: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = "GetAccessKeyStatus()"
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Message = fmt.Sprintf("Query failed with %d: %v\n", hydrusError.Errors.Code, resp.Body)
		hydrusError.Errors.State = false
	}
	return *accessKeyStatus, resp, *hydrusError
}

// GetTagServices returns tag services
// accessKey is hash from hydrus client
func (s *TagService) GetTagServices(accessKey string) (TagServices, *http.Response, HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function GetTagServices(%s)", accessKey)
		hydrusError.Errors.State = false
	}
	tagServices := new(TagServices)
	resp, err := s.sling.New().Path(fmt.Sprintf("add_tags/get_tag_services?Hydrus-Client-API-Access-Key=%s", accessKey)).Receive(tagServices, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			hydrusError.Errors.Function = "GetTagServices()"
			hydrusError.Message = fmt.Sprintf("Failed ioutil.ReadAll(): %v", err)
			hydrusError.Errors.State = false
		}

		fmt.Println("TagServices: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = "GetTagServices()"
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Message = fmt.Sprintf("Query failed with %d: %v\n", hydrusError.Errors.Code, resp.Body)
		hydrusError.Errors.State = false
	}
	return *tagServices, resp, *hydrusError
}

// GetURLInfo returns info about how hydrus will parse provided url
func (s *URLService) GetURLInfo(accessKey string, uri string) (URLInfo, *http.Response, HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("GetURLInfo(%s, %s)", accessKey, uri)
		hydrusError.Errors.State = false
	}
	if errs := validate.Var(uri, "url,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("uri: %+v, Error: %+v\n", uri, errs)
		hydrusError.Errors.Function = fmt.Sprintf("GetURLInfo(%s, %s)", accessKey, uri)
		hydrusError.Errors.State = false
	}
	urlInfo := new(URLInfo)
	resp, err := s.sling.New().Path(fmt.Sprintf("add_urls/get_url_info?Hydrus-Client-API-Access-Key=%s&url=%s", accessKey, url.QueryEscape(uri))).Receive(urlInfo, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			hydrusError.Errors.Function = "GetURLInfo()"
			hydrusError.Message = fmt.Sprintf("Failed ioutil.ReadAll(): %v", err)
			hydrusError.Errors.State = false
		}

		fmt.Println("URLInfo: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = "GetURLInfo()"
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Message = fmt.Sprintf("Query failed with %d: %v\n", hydrusError.Errors.Code, resp.Body)
		hydrusError.Errors.State = false
	}
	return *urlInfo, resp, *hydrusError
}

// GetURLFiles returns info about files associated with url
func (s *URLService) GetURLFiles(accessKey string, uri string) (URLFiles, *http.Response, HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("GetURLFiles(%s, %s)", accessKey, uri)
		hydrusError.Errors.State = false
	}
	if errs := validate.Var(uri, "url,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("uri: %+v, Error: %+v\n", uri, errs)
		hydrusError.Errors.Function = fmt.Sprintf("GetURLFiles(%s, %s)", accessKey, uri)
		hydrusError.Errors.State = false
	}
	urlFiles := new(URLFiles)

	resp, err := s.sling.New().Path(fmt.Sprintf("add_urls/get_url_files?Hydrus-Client-API-Access-Key=%s&url=%s", accessKey, url.QueryEscape(uri))).Receive(urlFiles, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			hydrusError.Errors.Function = fmt.Sprintf("GetURLFiles(%s, %s)", accessKey, uri)
			hydrusError.Message = fmt.Sprintf("Failed ioutil.ReadAll(): %v", err)
			hydrusError.Errors.State = false
		}

		fmt.Println("URLFiles: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = fmt.Sprintf("GetURLFiles(%s, %s)", accessKey, uri)
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Message = fmt.Sprintf("Query failed with %d: %v\n", hydrusError.Errors.Code, resp.Body)
		hydrusError.Errors.State = false
	}
	return *urlFiles, resp, *hydrusError
}

// GetFileMetaData returns info about files associated with url
func (s *FileService) GetFileMetaData(accessKey string, params FileMetaDataParams) (FileMetaData, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	fileMetaData := new(FileMetaData)
	var urlParams string
	if len(params.FileIds) != 0 {
		s := strings.Join(params.FileIds, ", ")
		urlParams = "[" + s + "]"
		urlParams = url.QueryEscape(urlParams)
		urlParams = "file_ids=" + urlParams
	}
	if len(params.Hashes) != 0 {
		s := strings.Join(params.Hashes, `","`)
		urlParams = "[" + fmt.Sprintf(`"%s"`, s) + "]"
		urlParams = url.QueryEscape(urlParams)
		urlParams = "hashes=" + urlParams
	}
	if params.OnlyReturnIdentifiers {
		urlParams = urlParams + "&only_return_identifiers=true"
	}
	if params.DetailedFURLInformation {
		urlParams = urlParams + "&detailed_url_information=true"
	}

	urlParams = urlParams + fmt.Sprintf("Hydrus-Client-API-Access-Key=%s", accessKey)
	hydrusError := new(HydrusError)
	finalURL := fmt.Sprintf("%sget_files/file_metadata?%s", baseURL, urlParams)
	resp, err := s.sling.New().Path(finalURL).Receive(fileMetaData, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Return MetaData: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *fileMetaData, resp, hydrusError
}

// SearchForFiles returns info about files associated with url
func (s *FileService) SearchForFiles(accessKey string, params FileSearchParams) (SearchFiles, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	searchResults := new(SearchFiles)
	hydrusError := new(HydrusError)
	var urlParams string
	if len(params.Tags) != 0 {
		s := strings.Join(params.Tags, `","`)
		urlParams = "[" + fmt.Sprintf(`"%s"`, s) + "]"
		urlParams = url.QueryEscape(urlParams)
		urlParams = "tags=" + urlParams
	}
	if params.SystemArchive {
		urlParams = urlParams + "&system_archive=true"
	}
	if params.SystemInbox {
		urlParams = urlParams + "&system_inbox=true"
	}
	urlParams = urlParams + fmt.Sprintf("Hydrus-Client-API-Access-Key=%s", accessKey)
	finalURL := fmt.Sprintf("%sget_files/search_files?%s", baseURL, urlParams)
	resp, err := s.sling.New().Path(finalURL).Receive(searchResults, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("FileSearch: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *searchResults, resp, hydrusError
}

// AddURLToHydrus adds a url to hydrus
//	addURLParams := new(AddURLParams)
//
//	addURLParams.URL = "https://8ch.net/tv/res/1846574.html"
//	addURLParams.DestinationPageName = "kino zone"
//	addURLParams.ShowDestinationPage = true
//	var tagMap = make(map[string][]string)
//	tagMap["my tags"] = []string{"hama", "bema"}
//	addURLParams.ServiceNamesToTags = tagMap
//	addURL, _, err := client.AddURLService.AddURLToHydrus(accessKey, *addURLParams)
//	if err != nil {
//		fmt.Println(err)
//	}
func (s *URLService) AddURLToHydrus(accessKey string, params AddURLParams) (AddURL, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	newURL := new(AddURL)
	hydrusError := new(HydrusError)
	s.sling.Set("Content-Type", "application/json")
	resp, err := s.sling.Post(fmt.Sprintf("add_urls/add_url?Hydrus-Client-API-Access-Key=%s", accessKey)).BodyJSON(&params).Receive(newURL, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Add URL: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *newURL, resp, hydrusError
}

// AddTagsToHydrus adds tags to hashes
//	addURLParams := new(AddURLParams)
//
//	addURLParams.URL = "https://8ch.net/tv/res/1846574.html"
//	addURLParams.DestinationPageName = "kino zone"
//	addURLParams.ShowDestinationPage = true
//	var tagMap = make(map[string][]string)
//	tagMap["my tags"] = []string{"hama", "bema"}
//	addURLParams.ServiceNamesToTags = tagMap
//	addURL, _, err := client.AddURLService.AddURLToHydrus(accessKey, *addURLParams)
//	if err != nil {
//		fmt.Println(err)
//	}
func (s *TagService) AddTagsToHydrus(accessKey string, params AddTagsParams) (AddTags, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	newTags := new(AddTags)
	hydrusError := new(HydrusError)
	s.sling.Set("Content-Type", "application/json")
	resp, err := s.sling.Post(fmt.Sprintf("add_tags/add_tags?Hydrus-Client-API-Access-Key=%s", accessKey)).BodyJSON(&params).Receive(newTags, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Add Tags to Hash: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *newTags, resp, hydrusError
}

// AddFileToHydrus adds a file to hydrus
//	addFileParams := new(AddFileParams)
//	addFileParams.Path = "E:\\to_import\\ayanami.jpg"
func (s *FileService) AddFileToHydrus(accessKey string, params AddURLParams) (AddFile, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	newFile := new(AddFile)
	hydrusError := new(HydrusError)
	s.sling.Set("Content-Type", "application/json")
	resp, err := s.sling.Post(fmt.Sprintf("add_files/add_file?Hydrus-Client-API-Access-Key=%s", accessKey)).BodyJSON(&params).Receive(newFile, hydrusError)
	//.Path("add_urls/add_url").Receive(newURL, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		} else {

			fmt.Println("Add File to Hydrus: ", string(responseData))
		}
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *newFile, resp, hydrusError
}

// CleanTags returns cleaned tags from user input
// takes apikey and []string
//
func (s *TagService) CleanTags(accessKey string, params []string) (CleanedTags, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	cleanedTags := new(CleanedTags)
	hydrusError := new(HydrusError)
	var urlParams string
	if len(params) != 0 {
		s := strings.Join(params, `","`)
		urlParams = "[" + fmt.Sprintf(`"%s"`, s) + "]"
		urlParams = url.QueryEscape(urlParams)
		urlParams = "tags=" + urlParams
	}
	urlParams = urlParams + fmt.Sprintf("&Hydrus-Client-API-Access-Key=%s", accessKey)
	finalURL := fmt.Sprintf("%sadd_tags/clean_tags?%s", baseURL, urlParams)
	resp, err := s.sling.New().Path(finalURL).Receive(cleanedTags, hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		} else {

			fmt.Println("Tag Cleaning: ", string(responseData))
		}
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *cleanedTags, resp, hydrusError
}

// AssociateURLToHash adds a file to hydrus
//	associateURL := new(AssociateURL)
//	associateURL.URL = "https://"
//	associateURL.Hash = "sha256"
//{
//	"url_to_add" : "https://rule34.xxx/index.php?id=2588418&page=post&s=view",
//	"hash" : "3b820114f658d768550e4e3d4f1dced3ff8db77443472b5ad93700647ad2d3ba"
//}
func (s *URLService) AssociateURLToHash(accessKey string, params AssociateURLParams) (*http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	hydrusError := new(HydrusError)
	s.sling.Set("Content-Type", "application/json")
	resp, err := s.sling.Post(fmt.Sprintf("add_urls/associate_url?Hydrus-Client-API-Access-Key=%s", accessKey)).BodyJSON(&params).ReceiveSuccess(hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		} else {
			fmt.Println("Associate URL: ", string(responseData))
		}
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return resp, hydrusError
}

// ReturnThumbnailByHash returns thumbnail file by hash
// takes apikey and int64
//
func (s *FileService) ReturnThumbnailByHash(accessKey string, id string) (io.Reader, *http.Response, *HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnThumbnailByHash(%s, %s)", accessKey, id)
		hydrusError.Errors.State = false
	}

	var thumbnailFile io.Reader
	var urlParams string

	if errs := validate.Var(id, "hexadecimal"); errs == nil {
		urlParams = fmt.Sprintf("%s=%v", "hash", id)
	} else {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnThumbnailByHash(%s, %s)", accessKey, id)
		hydrusError.Errors.State = false
	}
	urlParams = urlParams + fmt.Sprintf("&Hydrus-Client-API-Access-Key=%s", accessKey)
	finalURL := fmt.Sprintf("%sget_files/thumbnail?%s", baseURL, urlParams)
	hydrusError.Errors.APIURI = finalURL
	client := &http.Client{}
	req, err := http.NewRequest("GET", finalURL, nil)
	if err != nil {
		fmt.Printf("Failed to create new Request: %+v\n", err)
	}
	req.Header.Add("Hydrus-Client-API-Access-Key", accessKey)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to create new DO: %+v\n", err)
	}
	defer resp.Body.Close()

	thumbnailFileByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("ioutil.ReadAll -> %v", err)
	}
	thumbnailFile = bytes.NewReader(thumbnailFileByte)

	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnThumbnailByHash(%s, %s)", accessKey, id)
		hydrusError.Message = fmt.Sprintln("Did not return 200 status code.")
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Errors.State = false
	}

	return thumbnailFile, resp, hydrusError
}

// ReturnThumbnailByID returns thumbnail file by ID
// takes apikey and int64
//
func (s *FileService) ReturnThumbnailByID(accessKey string, id int64) (io.Reader, *http.Response, *HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnThumbnailByHash(%s, %d)", accessKey, id)
		hydrusError.Errors.State = false
	}

	var thumbnailFile io.Reader
	var urlParams string

	if errs := validate.Var(id, "numeric,required"); errs == nil {

		urlParams = fmt.Sprintf("%s=%v", "file_id", id)

	} else {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByID(%s, %d)", accessKey, id)
		hydrusError.Errors.State = false
	}
	urlParams = urlParams + fmt.Sprintf("&Hydrus-Client-API-Access-Key=%s", accessKey)
	finalURL := fmt.Sprintf("%sget_files/thumbnail?%s", baseURL, urlParams)
	hydrusError.Errors.APIURI = finalURL
	client := &http.Client{}
	req, err := http.NewRequest("GET", finalURL, nil)
	if err != nil {
		fmt.Printf("Failed to create new Request: %+v\n", err)
	}
	req.Header.Add("Hydrus-Client-API-Access-Key", accessKey)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to create new DO: %+v\n", err)
	}
	defer resp.Body.Close()

	thumbnailFileByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("ioutil.ReadAll -> %v", err)
	}
	thumbnailFile = bytes.NewReader(thumbnailFileByte)

	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByID(%s, %d)", accessKey, id)
		hydrusError.Message = fmt.Sprintln("Did not return 200 status code.")
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Errors.State = false
	}

	return thumbnailFile, resp, hydrusError
}

// ReturnFileByHash returns file by hash
// takes apikey and string or string
//
func (s *FileService) ReturnFileByHash(accessKey string, id string) (io.Reader, *http.Response, *HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByHash(%s, %s)", accessKey, id)
		hydrusError.Errors.State = false
	}

	var thumbnailFile io.Reader
	var urlParams string

	if errs := validate.Var(id, "hexadecimal"); errs == nil {
		urlParams = fmt.Sprintf("%s=%v", "hash", id)
	} else {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByHash(%s, %s)", accessKey, id)
		hydrusError.Errors.State = false
	}
	urlParams = urlParams + fmt.Sprintf("&Hydrus-Client-API-Access-Key=%s", accessKey)
	finalURL := fmt.Sprintf("%sget_files/file?%s", baseURL, urlParams)
	hydrusError.Errors.APIURI = finalURL
	client := &http.Client{}
	req, err := http.NewRequest("GET", finalURL, nil)
	if err != nil {
		fmt.Printf("Failed to create new Request: %+v\n", err)
	}
	req.Header.Add("Hydrus-Client-API-Access-Key", accessKey)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to create new DO: %+v\n", err)
	}
	defer resp.Body.Close()

	thumbnailFileByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("ioutil.ReadAll -> %v", err)
	}
	thumbnailFile = bytes.NewReader(thumbnailFileByte)

	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByHash(%s, %s)", accessKey, id)
		hydrusError.Message = fmt.Sprintln("Did not return 200 status code.")
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Errors.State = false
	}

	return thumbnailFile, resp, hydrusError
}

// ReturnFileByID returns file by ID
// takes apikey and int64 or string
//
func (s *FileService) ReturnFileByID(accessKey string, id int64) (io.Reader, *http.Response, *HydrusError) {
	validate = validator.New()
	hydrusError := new(HydrusError)
	hydrusError.Errors.State = true
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByID(%s, %d)", accessKey, id)
		hydrusError.Errors.State = false
	}

	var thumbnailFile io.Reader
	var urlParams string

	if errs := validate.Var(id, "numeric,required"); errs == nil {

		urlParams = fmt.Sprintf("%s=%v", "file_id", id)

	} else {
		hydrusError.Message = fmt.Sprintf("accessKey: %+v, Error: %+v\n", accessKey, errs)
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByID(%s, %d)", accessKey, id)
		hydrusError.Errors.State = false
	}
	urlParams = urlParams + fmt.Sprintf("&Hydrus-Client-API-Access-Key=%s", accessKey)
	finalURL := fmt.Sprintf("%sget_files/file?%s", baseURL, urlParams)
	hydrusError.Errors.APIURI = finalURL
	client := &http.Client{}
	req, err := http.NewRequest("GET", finalURL, nil)
	if err != nil {
		fmt.Printf("Failed to create new Request: %+v\n", err)
	}
	req.Header.Add("Hydrus-Client-API-Access-Key", accessKey)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to create new DO: %+v\n", err)
	}
	defer resp.Body.Close()

	thumbnailFileByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("ioutil.ReadAll -> %v", err)
	}
	thumbnailFile = bytes.NewReader(thumbnailFileByte)

	if resp.StatusCode != 200 {
		hydrusError.Errors.Function = fmt.Sprintf("Called Function ReturnFileByID(%s, %d)", accessKey, id)
		hydrusError.Message = fmt.Sprintln("Did not return 200 status code.")
		hydrusError.Errors.Code = resp.StatusCode
		hydrusError.Errors.State = false
	}

	return thumbnailFile, resp, hydrusError
}

//GetCurrentPages is a function to return json list of all currently open pages on client.
func (s *ManagePagesService) GetCurrentPages(accessKey string) (CurrentPages, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}

	currentPages := new(CurrentPages)
	hydrusError := new(HydrusError)
	resp, err := s.sling.New().Path(fmt.Sprintf("manage_pages/get_pages?Hydrus-Client-API-Access-Key=%s", accessKey)).ReceiveSuccess(&currentPages)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("CurrentPages: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *currentPages, resp, hydrusError
}

//GetPageInfo is a function to Get information about a specific page.
func (s *ManagePagesService) GetPageInfo(accessKey string) (CurrentPages, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}

	currentPages := new(CurrentPages)
	hydrusError := new(HydrusError)
	resp, err := s.sling.New().Path(fmt.Sprintf("manage_pages/get_page_info?Hydrus-Client-API-Access-Key=%s", accessKey)).ReceiveSuccess(&currentPages)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("CurrentPages: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *currentPages, resp, hydrusError
}

//FocusPage 'Show' a page in the main GUI, making it the current page in view. If it is already the current page, no change is made.
func (s *ManagePagesService) FocusPage(accessKey string) (CurrentPages, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}
	s.sling.Set("Content-Type", "application/json")

	currentPages := new(CurrentPages)
	hydrusError := new(HydrusError)
	resp, err := s.sling.New().Path(fmt.Sprintf("manage_pages/focus_page?Hydrus-Client-API-Access-Key=%s", accessKey)).ReceiveSuccess(&currentPages)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("CurrentPages: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *currentPages, resp, hydrusError
}

//GetCookies is a function to return json object of all cookies associated to domain.
func (s *ManageCookiesService) GetCookies(accessKey string, domain string) (DomainCookies, *http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}

	domainCookies := new(DomainCookies)
	hydrusError := new(HydrusError)
	resp, err := s.sling.New().Path(fmt.Sprintf("manage_cookies/get_cookies?Hydrus-Client-API-Access-Key=%s&domain=%s", accessKey, domain)).ReceiveSuccess(&domainCookies)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("CurrentPages: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *domainCookies, resp, hydrusError
}

//SetCookies is a function to send json object of cookies associated to domain.
//TODO: There be dragons
func (s *ManageCookiesService) SetCookies(accessKey string, domainCookies *DomainCookies) (*http.Response, error) {
	validate = validator.New()
	if errs := validate.Var(accessKey, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", accessKey, errs)
	}

	hydrusError := new(HydrusError)
	s.sling.Set("Content-Type", "application/json")
	resp, err := s.sling.Post(fmt.Sprintf("manage_cookies/get_cookies?Hydrus-Client-API-Access-Key=%s", accessKey)).BodyJSON(&domainCookies).ReceiveSuccess(hydrusError)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("CurrentPages: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		//fmt.Printf("Status Code: %v\n", resp.StatusCode)
		hydrusError.Message = fmt.Sprintf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return resp, hydrusError
}

// Client to wrap services

// Client is a tiny Github client
type Client struct {
	UtilsService         *UtilsService
	TagService           *TagService
	FileService          *FileService
	URLService           *URLService
	ManagePagesService   *ManagePagesService
	ManageCookiesService *ManageCookiesService
	// other service endpoints...
}

// NewClient returns a new Client
func NewClient(httpClient *http.Client) *Client {
	return &Client{
		UtilsService:         NewUtilsService(httpClient),
		TagService:           NewTagService(httpClient),
		FileService:          NewFileService(httpClient),
		URLService:           NewURLService(httpClient),
		ManagePagesService:   NewManagePagesService(httpClient),
		ManageCookiesService: NewManageCookiesService(httpClient),
	}
}
func init() {
	client := NewClient(nil)
	apiVersion, _, _ := client.UtilsService.GetAPIVersion()

	if apiVersion.Version != apiVersionCompat {
		log.Printf("!!!API version(v%d) not version supported(v%d)!!!\n It may still work but expect issues.\n", apiVersion.Version, apiVersionCompat)
	}
}

/*func main() {
	client := NewClient(nil)
	var files FileMetaDataParams
	var search FileSearchParams
	var tags CleanedTagsParams
	var requestAccess RequestNewPermissionsParams
	const accessKey = "ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a"

	versionNum, _, _ := client.UtilsService.GetAPIVersion()
	//accessStatus, _, _ := client.VerifyAccessKeyService.getAccessKeyStatus("ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a")
	//urlINFO, _, _ := client.URLService.GetURLInfo("ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a", "https://nhentai.net/g/270492/")
	//tagServices, _, _ := client.TagServicesService.getTagServices("ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a")
	search.Tags = []string{"skirt"}
	search.SystemArchive = true
	files.FileIds = []string{"1223", "666"}
	files.OnlyReturnIdentifiers = false
	tags.Tags = []string{"samus aran", "bill"}
	requestAccess.Name = "testing123"
	requestAccess.BasicPermissions = []string{"0", "1", "3"}

	//fileMetaData, _, _ := client.FileService.GetFileMetaData(accessKey, files)

	//searchFiles, _, _ := client.SearchFilesService.SearchForFiles("ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a", search)

	//cleaningTags, _, _ := client.TagService.CleanTags("ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a", tags)
	//newPermission, _, err := client.UtilsService.RequestPermissions(requestAccess)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//addURLParams := new(AddURLParams)

	//addURLParams.URL = "https://8ch.net/tv/res/1846574.html"
	//addURLParams.DestinationPageName = "kino zone"
	//addURLParams.ShowDestinationPage = true
	//var tagMap = make(map[string][]string)
	//tagMap["my tags"] = []string{"hama", "bema"}
	//addURLParams.ServiceNamesToTags = tagMap
	//addURL, _, err := client.AddURLService.AddURLToHydrus(accessKey, *addURLParams)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Printf("\n%v\n", addURL.HumanResultText)
	//dump, _ := httputil.DumpResponse(resp, true)
	//fmt.Printf("%v\n", dump)
	fmt.Printf("\nhydrus API version: %v\n", versionNum.Version)
	//fmt.Printf("\nRequested new access Key: %v\n", newPermission)
	//fmt.Printf("\nhydrus API version: %v\n", searchFiles.FileIds)
	//fmt.Printf("\n%v\n%s\n", accessStatus.BasicPermissions, accessStatus.HumanDescription)
	//fmt.Printf("\n%v\n", tagServices)
	//metaDataOne := fileMetaData.Metadata
	fmt.Println("--------------------------------------------------")
	//for i := range fileMetaData.Metadata {
	//	fmt.Printf("MetaData: %v\n", fileMetaData.Metadata[i])
	//}
	//fmt.Printf("\nURLFiles: %v\n", urlINFO.MatchName)
	//fmt.Printf("\nCleanedTags: %v\n", cleaningTags)
	//fmt.Printf("MetaData: %v\n", metaDataOne[0].Mime)
}*/
